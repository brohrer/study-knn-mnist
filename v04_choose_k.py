import numpy as np
import matplotlib.pyplot as plt
from cottonwood.knn import KNN
from cottonwood.structure import Structure
from cottonwood_data_mnist.mnist_block import TrainingData, TestingData

k = 3
# k = 3, 94.5 percent
n_iter_train = int(1e4)
n_iter_test = int(1e3)
n_iter_update = int(1e3)
training_figfile = "training.png"

model = Structure()
model.add(TrainingData(), "train")
model.add(KNN(k=k), "knn")

model.connect("train", "knn")
model.connect("train", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_train):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

    if (i_iter + 1) % n_iter_update == 0:

        recent_misses = np.array(misses[-n_iter_update:])
        accuracy = 1 - np.mean(recent_misses)
        accuracies.append(accuracy)
        print(
            f"{np.sum(recent_misses)} out of " +
            f"{recent_misses.size} digits misclassified " +
            f"for an accuracy of {accuracy * 100:.03} percent.")
        iterations = (np.arange(len(accuracies)) + 1) * n_iter_update

        fig = plt.figure()
        ax = fig.gca()
        ax.plot(iterations, accuracies, color="#04253a")
        ax.set_title(f"k-NN with MNIST digits, k = {model.blocks['knn'].k}")
        ax.set_xlabel("Iteration")
        ax.set_ylabel("Training accuracy")
        ax.grid()
        plt.savefig(training_figfile, dpi=300)
        plt.close()

model.remove("train")
model.add(TestingData(), "data")
model.connect("data", "knn")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)
model.blocks["knn"].max_points = n_iter_train

misses = []
accuracies = []
for i_iter in range(n_iter_test):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

misses = np.array(misses)
accuracy = 1 - np.mean(misses)
accuracies.append(accuracy)
print("Testing")
print(
    f"{np.sum(misses)} out of " +
    f"{misses.size} digits misclassified " +
    f"for an accuracy of {accuracy * 100:.03} percent.")

