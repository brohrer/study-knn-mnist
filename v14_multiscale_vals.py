import numpy as np
import matplotlib.pyplot as plt
from cottonwood.knn import KNN
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.structure import Structure
from cottonwood_data_mnist.mnist_block import TrainingData, TestingData
# from cottonwood.experimental.pixel_chunks import PixelChunks
# from cottonwood.operations_2d import Resample
from cottonwood.pooling import AvgPool2D
from conv2d_edges import Conv2DEdges
from cottonwood.experimental.visualize_conv2d_kernels import render_kernel

# 0% overlap
# pool 2 and 4 94.21%
# 1 and pool 2

k = 5
max_points = int(1e4)
n_iter_train = int(2e4)
n_iter_test = int(1e4)
n_iter_update = int(1e3)
training_figfile = f"training_k{k}_b2_c{max_points}.png"

model = Structure()
model.add(TrainingData(), "train")
model.add(Copy(), "copy")
model.add(AvgPool2D(window=2, stride=2), "pool_2")
model.add(AvgPool2D(window=4, stride=4), "pool_4")
# model.add(Conv2DEdges(kernel_size=3, n_kernels=4), "conv_2")
# model.add(Conv2DEdges(kernel_size=3, n_kernels=4), "conv_4")
model.add(Flatten(), "flatten_2")
model.add(Flatten(), "flatten_4")
model.add(Stack(), "stack")
model.add(KNN(k=k, max_points=max_points), "knn")

model.connect("train", "copy")
model.connect("copy", "pool_2", i_port_tail=0)
model.connect("copy", "pool_4", i_port_tail=1)
# model.connect("pool_2", "conv_2")
# model.connect("pool_4", "conv_4")
# model.connect("copy", "conv_4", i_port_tail=1)
# model.connect("conv_2", "flatten_2")
# model.connect("conv_4", "flatten_4")
model.connect("pool_2", "flatten_2")
model.connect("pool_4", "flatten_4")
model.connect("flatten_2", "stack", i_port_head=0)
model.connect("flatten_4", "stack", i_port_head=1)
# model.connect("stack", "knn")
# model.connect("train", "pool_2")
# model.connect("pool_2", "conv")
# model.connect("train", "pool_4")
# model.connect("pool_4", "conv")
model.connect("stack", "knn")
model.connect("train", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_train):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

    if (i_iter + 1) % n_iter_update == 0:

        '''
        for i_kernel in range(4):
            render_kernel(
                model.blocks["conv_2"],
                i_kernel,
                model.blocks["conv_2"].forward_in,
                ".",
                f"kernel_{i_kernel}.png",
            )
        '''

        recent_misses = np.array(misses[-n_iter_update:])
        accuracy = 1 - np.mean(recent_misses)
        accuracies.append(accuracy)
        print(
            f"{np.sum(recent_misses)} out of " +
            f"{recent_misses.size} digits misclassified " +
            f"at iteration {i_iter + 1} " +
            f"for an accuracy of {accuracy * 100:.04} percent.")
        iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
        errors = (1 - np.array(accuracies)) * 100

        fig = plt.figure()
        ax = fig.gca()
        ax.plot(iterations, errors, color="#04253a")
        ax.set_title(
            "k-NN with MNIST digits, " +
            f"k = {model.blocks['knn'].k}")
        ax.set_xlabel("Iteration")
        ax.set_ylabel("Training error")
        ax.set_ylim(0, 25)
        ax.grid()
        plt.savefig(training_figfile, dpi=300)
        plt.close()

model.remove("train")
model.add(TestingData(), "data")
model.connect("data", "copy")
# model.connect("data", "pool_4")
# model.connect("data", "pool_2")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_test):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

misses = np.array(misses)
accuracy = 1 - np.mean(misses)
accuracies.append(accuracy)
print("Testing")
print(
    f"{np.sum(misses)} out of " +
    f"{misses.size} digits misclassified " +
    f"for an accuracy of {accuracy * 100:.04} percent.")


