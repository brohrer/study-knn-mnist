import numpy as np
import matplotlib.pyplot as plt
# from cottonwood.knn import KNN
from cottonwood.structure import Structure
from cottonwood_data_mnist.mnist_block import TrainingData, TestingData

from cottonwood.experimental.multiview_knn import MultiviewKNN
from cottonwood.experimental.translation_views import TranslationViews

# k = 5, pixels = 0, max = 1e3, 94.54%
# k = 5, pixels = 1, max = 1e3, 95.31%

n_pixels = 1
k = 5
max_points = int(1e4)
n_iter_train = int(5e4)
n_iter_test = int(1e4)
n_iter_update = int(1e3)
training_figfile = f"training_k{k}_p{n_pixels}_c{max_points}.png"

model = Structure()
model.add(TrainingData(), "train")
model.add(TranslationViews(max_pixel_shift=n_pixels), "translations")
model.add(MultiviewKNN(k=k, max_points=max_points), "knn")

model.connect("train", "translations")
model.connect("translations", "knn")
model.connect("train", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_train):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

    if (i_iter + 1) % n_iter_update == 0:

        recent_misses = np.array(misses[-n_iter_update:])
        accuracy = 1 - np.mean(recent_misses)
        accuracies.append(accuracy)
        print(
            f"{np.sum(recent_misses)} out of " +
            f"{recent_misses.size} digits misclassified " +
            f"at iteration {i_iter + 1} " +
            f"for an accuracy of {accuracy * 100:.04} percent.")
        iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
        errors = (1 - np.array(accuracies)) * 100

        fig = plt.figure()
        ax = fig.gca()
        ax.plot(iterations, errors, color="#04253a")
        ax.set_title(
            "k-NN with MNIST digits, " +
            f"k = {model.blocks['knn'].k}, "
            f"max pixel shift = {n_pixels}")
        ax.set_xlabel("Iteration")
        ax.set_ylabel("Training error")
        ax.set_ylim(0, 25)
        ax.grid()
        plt.savefig(training_figfile, dpi=300)
        plt.close()

model.remove("train")
model.add(TestingData(), "data")
model.connect("data", "translations")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)
# model.blocks["knn"].max_points = n_iter_train

misses = []
accuracies = []
for i_iter in range(n_iter_test):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

misses = np.array(misses)
accuracy = 1 - np.mean(misses)
accuracies.append(accuracy)
print("Testing")
print(
    f"{np.sum(misses)} out of " +
    f"{misses.size} digits misclassified " +
    f"for an accuracy of {accuracy * 100:.04} percent.")
