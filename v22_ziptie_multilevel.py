import os
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.structure import Structure
from cottonwood_data_mnist.mnist_block import TrainingData, TestingData
from cottonwood.experimental.multiview_knn import MultiviewKNN
from cottonwood.experimental.pixel_neighborhood_views \
    import PixelNeighborhoodViews
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.pooling import AvgPool2D

# neighborhood_size = 9

# kNN parameters
k = 11
max_node_points = int(1e4)

# ziptie parameters
n_bundles = 1e2
threshold = 1e4

testing = True
if testing:
    n_iter_ziptie_train_0 = int(0)
    n_iter_ziptie_train_1 = int(0)
    n_iter_ziptie_train_2 = int(0)
    n_iter_ziptie_train_3 = int(1e6)
else:
    n_iter_ziptie_train_0 = int(3e5)
    n_iter_ziptie_train_1 = int(3e6)
    n_iter_ziptie_train_2 = int(1e7)
    n_iter_ziptie_train_3 = int(5e7)
n_iter_knn_train = int(5e4)
n_iter_test = int(1e4)
n_iter_update = int(1e3)
training_figfile = f"training.png"

model = Structure()
model.add(TrainingData(), "train")
model.add(AvgPool2D(window=2, stride=2), "pool")
model.add(Flatten(), "flatten")
# model.add(PixelNeighborhoodViews(
#     neighborhood_size=neighborhood_size), "neighborhoods")
model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_0")

model.connect("train", "pool")
# model.connect("pool", "neighborhoods")
model.connect("pool", "flatten")
model.connect("flatten", "ziptie_0")

def show_bundles(zt0, zt1, zt2, zt3):

    if zt3.n_bundles == 0:
        return
    # Select some bundles to visualize.
    n_bundle_viz = np.minimum(10, zt3.n_bundles)
    i_bundles = np.random.choice(
        zt3.n_bundles, size=n_bundle_viz, replace=False)
    for i_bundle in i_bundles:

        # Create a single active bundle signal.
        bundle_activities_3 = np.zeros(zt3.n_bundles)
        bundle_activities_3[int(i_bundle)] = 1

        # Get the corresponding cable (input) activities.
        bundle_activities_2 = zt3.project_bundle_activities(bundle_activities_3)
        bundle_activities_1 = zt2.project_bundle_activities(bundle_activities_2)
        bundle_activities_0 = zt1.project_bundle_activities(bundle_activities_1)
        activities = zt0.project_bundle_activities(bundle_activities_0)
        img_size = int(np.sqrt(activities.size))
        img = np.reshape(activities, (img_size, img_size))

        fig = plt.figure()
        ax = fig.gca()
        ax.imshow(img, cmap="gray")
        plt.savefig(os.path.join("images", f"bundle_{i_bundle}.png"))
        plt.close()

for i_iter in range(n_iter_ziptie_train_0):
    model.forward_pass()
    if i_iter % n_iter_update == 0:
        print(
            model.blocks["ziptie_0"].algo.n_bundles,
            "z0 bundles at iter",
            i_iter)

model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_1")
model.connect("ziptie_0", "ziptie_1")

for i_iter in range(n_iter_ziptie_train_1):
    model.forward_pass()
    if i_iter % n_iter_update == 0:
        print(
            model.blocks["ziptie_1"].algo.n_bundles,
            "z1 bundles at iter",
            i_iter)

model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_2")
model.connect("ziptie_1", "ziptie_2")

for i_iter in range(n_iter_ziptie_train_2):
    model.forward_pass()
    if i_iter % n_iter_update == 0:
        print(
            model.blocks["ziptie_2"].algo.n_bundles,
            "z2 bundles at iter",
            i_iter)

model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_3")
model.connect("ziptie_2", "ziptie_3")

for i_iter in range(n_iter_ziptie_train_3):
    model.forward_pass()
    if i_iter % n_iter_update == 0:
        print(
            model.blocks["ziptie_3"].algo.n_bundles,
            "z3 bundles at iter",
            i_iter)


model.add(MultiviewKNN(k=k, max_node_points=max_node_points), "knn")
model.connect("ziptie_3", "knn")
model.connect("train", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_knn_train):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

    if (i_iter + 1) % n_iter_update == 0:

        recent_misses = np.array(misses[-n_iter_update:])
        accuracy = 1 - np.mean(recent_misses)
        accuracies.append(accuracy)
        print(
            f"{np.sum(recent_misses)} out of " +
            f"{recent_misses.size} digits misclassified " +
            f"at iteration {i_iter + 1} " +
            f"for an accuracy of {accuracy * 100:.04} percent.")
        iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
        errors = (1 - np.array(accuracies)) * 100

        fig = plt.figure()
        ax = fig.gca()
        ax.plot(iterations, errors, color="#04253a")
        ax.set_title(
            "k-NN with MNIST digits, " +
            f"k = {model.blocks['knn'].knn.k}, " +
            # f"neighborhood size = {neighborhood_size}, " +
            f"max node points = {max_node_points}")
        ax.set_xlabel("Iteration")
        ax.set_ylabel("Training error")
        ax.set_ylim(0, 50)
        ax.grid()
        plt.savefig(training_figfile, dpi=300)
        plt.close()

        show_bundles(
            model.blocks["ziptie_0"].algo,
            model.blocks["ziptie_1"].algo,
            model.blocks["ziptie_2"].algo,
            model.blocks["ziptie_3"].algo)

model.remove("train")
model.add(TestingData(), "data")
model.connect("data", "pool")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_test):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

misses = np.array(misses)
accuracy = 1 - np.mean(misses)
accuracies.append(accuracy)
print("Testing")
print(
    f"{np.sum(misses)} out of " +
    f"{misses.size} digits misclassified " +
    f"for an accuracy of {accuracy * 100:.04} percent.")
