# study-knn-mnist

Use k-nearest neighbors to classify
[MNIST digits](https://en.wikipedia.org/wiki/MNIST_database).

This demo makes use of two other packages, the
[Cottonwood machine learning framework](https://e2eml.school/cottonwood)
and [the MNIST digits](https://gitlab.com/brohrer/cottonwood-data-mnist) data set, formatted for Cottonwood.

To install them at the command line, navigate to where you want them
to live and run

```bash
git clone https://gitlab.com/brohrer/cottonwood.git
git clone https://gitlab.com/brohrer/cottonwood-data-mnist.git
python3 -m pip install -e cottonwood
python3 -m pip install -e cottonwood-data-mnist
```

Cottonwood changes over time. Make sure you have the right version.

```bash
cd cottonwood
git checkout v33
```

Then run any of the v## scripts, for instance

```bash
python3 v05_evaluate_k_full.py
```
