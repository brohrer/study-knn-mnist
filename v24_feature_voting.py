import os
import pickle as pkl
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.structure import Structure
from cottonwood_data_mnist.mnist_block import TrainingData, TestingData
# from cottonwood.knn import KNN
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.experimental.feature_voting import FeatureVoting
from cottonwood.pooling import AvgPool2D

# model_filename = "model.pkl"
model_filename = "model_1e2.pkl"
# model_filename = "model_1e3.pkl"
training_figfile = "training.png"


troubleshooting = False
if troubleshooting:
    # For a quick run through the code
    n_iter_train = int(1e4)
    n_iter_test = int(1e3)
    n_iter_update = int(1e3)

    # ziptie parameters
    n_bundles = 1e2
    threshold = 10

else:
    n_iter_train = int(5e4)
    n_iter_test = int(1e4)
    n_iter_update = int(1e3)

    # ziptie parameters
    n_bundles = 1e2
    threshold = 1e1


def main():
    if os.path.exists(model_filename):
        model = retrieve_model(model_filename)
    else:
        model = initialize_model()
        train_unsupervised(model)
        save_model(model, model_filename)

    train_supervised(model)
    test_supervised(model)


def retrieve_model(filename):
    with open(filename, "rb") as f:
        model = pkl.load(f)
    model.add(TrainingData(), "training_data")
    model.connect("training_data", "pool")
    return model


def save_model(model, filename):
    model.remove("training_data")
    with open(filename, "wb") as f:
        pkl.dump(model, f)


def initialize_model():
    model = Structure()
    model.add(TrainingData(), "training_data")
    model.add(AvgPool2D(window=2, stride=2), "pool")
    model.add(Flatten(), "flatten")
    model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_0")

    model.connect("training_data", "pool")
    model.connect("pool", "flatten")
    model.connect("flatten", "ziptie_0")
    return model


def train_unsupervised(model):
    i_iter = 0
    while not model.blocks["ziptie_0"].is_full():
        i_iter += 1
        model.forward_pass()
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_0"].algo.n_bundles,
                "z0 bundles at iter",
                i_iter)
    model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_1")
    model.connect("ziptie_0", "ziptie_1")
    while not model.blocks["ziptie_1"].is_full():
        i_iter += 1
        model.forward_pass()
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_1"].algo.n_bundles,
                "z1 bundles at iter",
                i_iter)
    model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_2")
    model.connect("ziptie_1", "ziptie_2")
    while not model.blocks["ziptie_2"].is_full():
        i_iter += 1
        model.forward_pass()
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_2"].algo.n_bundles,
                "z2 bundles at iter",
                i_iter)

    model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie_3")
    model.connect("ziptie_2", "ziptie_3")

    while not model.blocks["ziptie_3"].is_full():
        i_iter += 1
        model.forward_pass()
        if i_iter % n_iter_update == 0:
            print(
                model.blocks["ziptie_3"].algo.n_bundles,
                "z3 bundles at iter",
                i_iter)
    return model


def train_supervised(model):

    model.add(TrainingData(), "training_data")
    model.add(FeatureVoting(n_categories=10), "voting")
    model.connect("training_data", "pool")
    model.connect("training_data", "voting", i_port_tail=1, i_port_head=1)
    model.connect("ziptie_3", "voting")

    misses = []
    accuracies = []
    for i_iter in range(n_iter_train):
        model.forward_pass()
        try:
            if (int(model.blocks["voting"].category) ==
                    model.blocks["voting"].forward_out):
                misses.append(0)
            else:
                misses.append(1)
        except TypeError:
            # Initially some of the estimates are None. Ignore these
            pass

        if (i_iter + 1) % n_iter_update == 0:

            recent_misses = np.array(misses[-n_iter_update:])
            accuracy = 1 - np.mean(recent_misses)
            accuracies.append(accuracy)
            print(
                f"{np.sum(recent_misses)} out of " +
                f"{recent_misses.size} digits misclassified " +
                f"at iteration {i_iter + 1} " +
                f"for an accuracy of {accuracy * 100:.04} percent.")
            iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
            errors = (1 - np.array(accuracies)) * 100

            fig = plt.figure()
            ax = fig.gca()
            ax.plot(iterations, errors, color="#04253a")
            ax.set_title("Feature voting with MNIST digits")
            ax.set_xlabel("Iteration")
            ax.set_ylabel("Training error")
            ax.set_ylim(0, 50)
            ax.grid()
            plt.savefig(training_figfile, dpi=300)
            plt.close()

            # show_bundles(
            #     model.blocks["ziptie_0"].algo,
            #     model.blocks["ziptie_1"].algo,
            #     model.blocks["ziptie_2"].algo,
            #     model.blocks["ziptie_3"].algo)
    return model


def test_supervised(model):
    model.remove("training_data")
    model.add(TestingData(), "testing_data")
    model.connect("testing_data", "pool")
    model.connect("testing_data", "voting", i_port_tail=1, i_port_head=1)

    misses = []
    accuracies = []
    for i_iter in range(n_iter_test):
        model.forward_pass()
        try:
            if (int(model.blocks["voting"].category) ==
                    model.blocks["voting"].forward_out):
                misses.append(0)
            else:
                misses.append(1)
        except TypeError:
            # Initially some of the estimates are None. Ignore these
            pass

    misses = np.array(misses)
    accuracy = 1 - np.mean(misses)
    accuracies.append(accuracy)
    print("Testing")
    print(
        f"{np.sum(misses)} out of " +
        f"{misses.size} digits misclassified " +
        f"for an accuracy of {accuracy * 100:.04} percent.")
    return model


def show_bundles(zt0, zt1, zt2, zt3):

    if zt3.n_bundles == 0:
        return
    # Select some bundles to visualize.
    n_bundle_viz = np.minimum(10, zt3.n_bundles)
    i_bundles = np.random.choice(
        zt3.n_bundles, size=n_bundle_viz, replace=False)
    for i_bundle in i_bundles:

        # Create a single active bundle signal.
        bundle_activities_3 = np.zeros(zt3.n_bundles)
        bundle_activities_3[int(i_bundle)] = 1

        # Get the corresponding cable (input) activities.
        bundle_activities_2 = zt3.project_bundle_activities(bundle_activities_3)
        bundle_activities_1 = zt2.project_bundle_activities(bundle_activities_2)
        bundle_activities_0 = zt1.project_bundle_activities(bundle_activities_1)
        activities = zt0.project_bundle_activities(bundle_activities_0)
        img_size = int(np.sqrt(activities.size))
        img = np.reshape(activities, (img_size, img_size))

        fig = plt.figure()
        ax = fig.gca()
        ax.imshow(img, cmap="gray")
        plt.savefig(os.path.join("images", f"bundle_{i_bundle}.png"))
        plt.close()


if __name__ == "__main__":
    main()
