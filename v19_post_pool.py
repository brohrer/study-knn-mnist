import numpy as np
import matplotlib.pyplot as plt
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.structure import Structure
from cottonwood_data_mnist.mnist_block import TrainingData, TestingData
from cottonwood.experimental.multiview_knn import MultiviewKNN
from cottonwood.experimental.pixel_neighborhood_views \
    import PixelNeighborhoodViews
from cottonwood.pooling import AvgPool2D
from conv2d_edges import Conv2DEdges

# n = 11, p = 1e3, 

neighborhood_size = 11
k = 11
max_node_points = int(1e4)
n_iter_train = int(5e4)
n_iter_test = int(1e4)
n_iter_update = int(1e3)
training_figfile = f"training_n{neighborhood_size}_p{max_node_points}_post.png"

model = Structure()
model.add(TrainingData(), "train")
model.add(AvgPool2D(window=4, stride=2), "pool")
# model.add(AvgPool2D(window=2, stride=2), "pool")
model.add(Conv2DEdges(kernel_size=3, n_kernels=4), "conv")
model.add(PixelNeighborhoodViews(
    neighborhood_size=neighborhood_size), "neighborhoods")
model.add(MultiviewKNN(k=k, max_node_points=max_node_points), "knn")
model.connect("train", "conv")
model.connect("conv", "pool")
model.connect("pool", "neighborhoods")
model.connect("neighborhoods", "knn")
model.connect("train", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_train):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

    if (i_iter + 1) % n_iter_update == 0:

        recent_misses = np.array(misses[-n_iter_update:])
        accuracy = 1 - np.mean(recent_misses)
        accuracies.append(accuracy)
        print(
            f"{np.sum(recent_misses)} out of " +
            f"{recent_misses.size} digits misclassified " +
            f"at iteration {i_iter + 1} " +
            f"for an accuracy of {accuracy * 100:.04} percent.")
        iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
        errors = (1 - np.array(accuracies)) * 100

        fig = plt.figure()
        ax = fig.gca()
        ax.plot(iterations, errors, color="#04253a")
        ax.set_title(
            "k-NN with MNIST digits, " +
            f"k = {model.blocks['knn'].knn.k}, " +
            f"neighborhood size = {neighborhood_size}, " +
            f"max node points = {max_node_points}")
        ax.set_xlabel("Iteration")
        ax.set_ylabel("Training error")
        ax.set_ylim(0, 10)
        ax.grid()
        plt.savefig(training_figfile, dpi=300)
        plt.close()

model.remove("train")
model.add(TestingData(), "data")
model.connect("data", "conv")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_test):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

misses = np.array(misses)
accuracy = 1 - np.mean(misses)
accuracies.append(accuracy)
print("Testing")
print(
    f"{np.sum(misses)} out of " +
    f"{misses.size} digits misclassified " +
    f"for an accuracy of {accuracy * 100:.04} percent.")
