import os
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.structure import Structure
from cottonwood_data_mnist.mnist_block import TrainingData, TestingData
from cottonwood.experimental.multiview_knn import MultiviewKNN
from cottonwood.experimental.pixel_neighborhood_views \
    import PixelNeighborhoodViews
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.pooling import AvgPool2D

# neighborhood_size = 9

# kNN parameters
k = 11
max_node_points = int(1e4)

# ziptie parameters
n_bundles = 1e2
threshold = 1e4

n_iter_ziptie_train = int(2e5)
# n_iter_ziptie_train = int(1e6)
n_iter_knn_train = int(5e4)
n_iter_test = int(1e4)
n_iter_update = int(1e3)
training_figfile = f"training.png"

model = Structure()
model.add(TrainingData(), "train")
model.add(AvgPool2D(window=2, stride=2), "pool")
model.add(Flatten(), "flatten")
# model.add(PixelNeighborhoodViews(
#     neighborhood_size=neighborhood_size), "neighborhoods")
model.add(Ziptie(n_outputs=n_bundles, threshold=threshold), "ziptie")

model.connect("train", "pool")
# model.connect("pool", "neighborhoods")
# model.connect("neighborhoods", "ziptie")
model.connect("pool", "flatten")
model.connect("flatten", "ziptie")

def show_bundles(zt):
    print(zt.n_bundles)

    if zt.n_bundles > 0:
        # Select some bundles to visualize.
        n_bundle_viz = np.minimum(10, zt.n_bundles)
        i_bundles = np.random.choice(
            zt.n_bundles, size=n_bundle_viz, replace=False)
        for i_bundle in i_bundles:

            # Create a single active bundle signal.
            bundle_activity = np.zeros(zt.n_bundles)
            bundle_activity[int(i_bundle)] = 1

            # Get the corresponding cable (input) activities.
            activities = zt.project_bundle_activities(bundle_activity)
            # print("i bundle", i_bundle)
            # print("activities", activities)
            # print("i activities", np.where(activities))
            img_size = int(np.sqrt(activities.size))
            img = np.reshape(activities, (img_size, img_size))

            fig = plt.figure()
            ax = fig.gca()
            ax.imshow(img, cmap="gray")
            plt.savefig(os.path.join("images", f"bundle_{i_bundle}.png"))
            plt.close()

for i_iter in range(n_iter_ziptie_train):
    model.forward_pass()
    if i_iter % n_iter_update == 0:
        print(model.blocks["ziptie"].algo.n_bundles, "bundles at iter", i_iter)
        #     show_bundles(model.blocks["ziptie"].algo)

model.add(MultiviewKNN(k=k, max_node_points=max_node_points), "knn")
model.connect("ziptie", "knn")
model.connect("train", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_knn_train):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

    if (i_iter + 1) % n_iter_update == 0:

        recent_misses = np.array(misses[-n_iter_update:])
        accuracy = 1 - np.mean(recent_misses)
        accuracies.append(accuracy)
        print(
            f"{np.sum(recent_misses)} out of " +
            f"{recent_misses.size} digits misclassified " +
            f"at iteration {i_iter + 1} " +
            f"for an accuracy of {accuracy * 100:.04} percent.")
        iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
        errors = (1 - np.array(accuracies)) * 100

        fig = plt.figure()
        ax = fig.gca()
        ax.plot(iterations, errors, color="#04253a")
        ax.set_title(
            "k-NN with MNIST digits, " +
            f"k = {model.blocks['knn'].knn.k}, " +
            # f"neighborhood size = {neighborhood_size}, " +
            f"max node points = {max_node_points}")
        ax.set_xlabel("Iteration")
        ax.set_ylabel("Training error")
        ax.set_ylim(0, 50)
        ax.grid()
        plt.savefig(training_figfile, dpi=300)
        plt.close()

        show_bundles(model.blocks["ziptie"].algo)

model.remove("train")
model.add(TestingData(), "data")
model.connect("data", "pool")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_test):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

misses = np.array(misses)
accuracy = 1 - np.mean(misses)
accuracies.append(accuracy)
print("Testing")
print(
    f"{np.sum(misses)} out of " +
    f"{misses.size} digits misclassified " +
    f"for an accuracy of {accuracy * 100:.04} percent.")
