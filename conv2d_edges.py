import numpy as np
from cottonwood.conv2d import Conv2D


class Conv2DEdges(Conv2D):
    """
    Like Conv2D but with hard-coded edge detecting kernels.
    """
    def initialize(self):
        """
        Weights is 4 dimensional
        Dimension 0 ~ kernel rows (kernel_size)
        Dimension 1 ~ kernel columns (kernel_size)
        Dimension 2 ~ input channels (n_channels)
        Dimension 3 ~ output channels (n_kernels)
        """
        self.n_rows_in, self.n_cols_in, self.n_channels = self.forward_in.shape

        self.n_rows_out = self.n_rows_in - self.kernel_size + 1
        self.n_cols_out = self.n_cols_in - self.kernel_size + 1

        self.weights = np.zeros((
            self.kernel_size,
            self.kernel_size,
            self.n_channels,
            self.n_kernels))

        for i_channel in range(self.n_channels):
            # Vertical edges
            self.weights[:, :, i_channel, 0] = np.array([
                [-1, 0, 1],
                [-1, 0, 1],
                [-1, 0, 1]])

            # Horizontal edges
            self.weights[:, :, i_channel, 1] = np.array([
                [ 1,  1,  1],
                [ 0,  0,  0],
                [-1, -1, -1]])

            # Right leaning edges
            self.weights[:, :, i_channel, 2] = np.array([
                [-1, -1, 0],
                [-1,  0, 1],
                [ 0,  1, 1]])

            # Left leaning edges
            self.weights[:, :, i_channel, 3] = np.array([
                [ 0,  1, 1],
                [-1,  0, 1],
                [-1, -1, 0]])


    def backward_pass(self, backward_in):
        """
        Propagate the outputs back through the layer.
        """
        self.backward_in = backward_in
        self.backward_out = self.backward_in
