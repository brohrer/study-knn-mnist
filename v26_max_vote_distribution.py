import os
import pickle as pkl
import numpy as np
import matplotlib.pyplot as plt
from cottonwood.operations import Copy, Flatten, Stack
from cottonwood.structure import Structure
from cottonwood_data_mnist.mnist_block import TrainingData, TestingData
from cottonwood.experimental.ziptie_block import Ziptie
from cottonwood.experimental.feature_voting import FeatureVoting
from cottonwood.pooling import AvgPool2D

model_filename = "model_super.pkl"

with open(model_filename, "rb") as f:
    model = pkl.load(f)

obs = model.blocks["voting"].observations
feat = model.blocks["voting"].feature_counts

feature_votes = obs / feat[:, np.newaxis]
max_votes = np.max(feature_votes, axis=1)
plt.hist(max_votes)
plt.show()
