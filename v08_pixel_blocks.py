import numpy as np
import matplotlib.pyplot as plt
from cottonwood.knn import KNN
from cottonwood.structure import Structure
from cottonwood_data_mnist.mnist_block import TrainingData, TestingData

from cottonwood.experimental.pixel_chunks import PixelChunks

# chunk size 2, 94.31%
# chunk size 4, 94.09%

chunk_size = 4
stride = 2
k = 5
max_points = int(1e4)
n_iter_train = int(2e4)
n_iter_test = int(1e4)
n_iter_update = int(1e3)
training_figfile = f"training_k{k}_b2_c{max_points}.png"

model = Structure()
model.add(TrainingData(), "train")
model.add(PixelChunks(chunk_size=chunk_size, stride=stride), "chunking")
model.add(KNN(k=k, max_points=max_points), "knn")

model.connect("train", "chunking")
model.connect("chunking", "knn")
model.connect("train", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_train):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

    if (i_iter + 1) % n_iter_update == 0:

        recent_misses = np.array(misses[-n_iter_update:])
        accuracy = 1 - np.mean(recent_misses)
        accuracies.append(accuracy)
        print(
            f"{np.sum(recent_misses)} out of " +
            f"{recent_misses.size} digits misclassified " +
            f"at iteration {i_iter + 1} " +
            f"for an accuracy of {accuracy * 100:.04} percent.")
        iterations = (np.arange(len(accuracies)) + 1) * n_iter_update
        errors = (1 - np.array(accuracies)) * 100

        fig = plt.figure()
        ax = fig.gca()
        ax.plot(iterations, errors, color="#04253a")
        ax.set_title(
            "k-NN with MNIST digits, " +
            f"k = {model.blocks['knn'].k}")
        ax.set_xlabel("Iteration")
        ax.set_ylabel("Training error")
        ax.set_ylim(0, 25)
        ax.grid()
        plt.savefig(training_figfile, dpi=300)
        plt.close()

model.remove("train")
model.add(TestingData(), "data")
model.connect("data", "chunking")
model.connect("data", "knn", i_port_tail=1, i_port_head=1)

misses = []
accuracies = []
for i_iter in range(n_iter_test):
    model.forward_pass()
    try:
        if model.blocks["knn"].target_label == model.blocks["knn"].forward_out:
            misses.append(0)
        else:
            misses.append(1)
    except TypeError:
        # Initially some of the estimates are None. Ignore these
        pass

misses = np.array(misses)
accuracy = 1 - np.mean(misses)
accuracies.append(accuracy)
print("Testing")
print(
    f"{np.sum(misses)} out of " +
    f"{misses.size} digits misclassified " +
    f"for an accuracy of {accuracy * 100:.04} percent.")
